<?php
require 'phpQuery.php';
require __DIR__ . '/vendor/autoload.php';

use HeadlessChromium\BrowserFactory;
/*
 * Парсер отелей из букинга в CSV/Json
 * Данный парсер настроен на работу только с контенотом booking.com
 * За основной класс взяли phpQuery
*/

Class parserBooking extends phpQuery{

    /*default параметры*/
    public $site_name = "Booking";
    public $site_url = "https://www.booking.com";
    public $site_search_page = "/searchresults.ru.html?";
    public $arResult = array();
    public $page = 1;
    public $quantity = 25;
    public $type_file = "json";
    public $arTypeFile = array("json", "csv");
    public $upload_path = "./upload/";
    public $upload_img = false;
    public $log;
    public $linkJsonFileID = "/ibby_parser/locationID.json";
    /*Селекторы парсера*/
    protected $selectorHotel = array(
        "LIST_PAGE" => array(
            "LIST" => "#hotellist_inner .sr_item_default",
            "LINK" => ".hotel_name_link",
            "LINK_REMOVE" => ".bui-link span",
            "PREVIEW_TEXT" =>".hotel_desc",
            "HOTEL_AREA" => ".bui-link",
            "CITY" => "#frm input[name='ssne']",
            "COUNT_PAGE" => ".bui-pagination__list .bui-pagination__list li:last a .bui-u-inline",
        ),
        "DETAIL_PAGE" => array(
            "HOTEL_ID" => "#right .hp-lists",
            "TYPE" => "#right #hp_hotel_name span",
            "CITY" => "input[name='ssne']",
            "NAME" => "#right #hp_hotel_name",
            "ADDRESS" => "#right #showMap2 span",
            "COORDINATES" => "#right #hotel_address",
            "STARS" => ".hp__hotel_ratings__stars .invisible_spoken",
            "DETAIL_TEXT" => "#right #property_description_content",
            "REMOVE_SPAN" => "#right #hp_hotel_name span",
            "REMOVE_TOOLTIP" => "#right #showMap2 span.show_map_endorsements_tooltip",
            "TYPE_IMAGES" => "#photos_distinct",
            "IMAGES_SLIDER" => "#photos_distinct a",
            "IMAGES_PLITKA" => "#hotel_main_content .bh-photo-grid-item:not(:last)",
            "POPULAR_FACILITIES" => ".hp_desc_important_facilities:first>div",
            "POPULAR_FACILITIES_REMOVE_SVG" => "svg",
            "CONDITIONS_LIST" => "#right #hotelPoliciesInc .description",
            "CONDITIONS_NAME" => ".policy_name span:last",
            "CONDITIONS_REMOVE" => ".policy_name",
            "CONDITIONS_CARD" => ".hp_bp_payment_method .payment_methods_overall button",
            "PROPERTY_LIST" => "#right .facilitiesChecklist .facilitiesChecklistSection",
            "PROPERTY_NAME" => "h5",
            "PROPERTY_REMOVE_SPAN_NAME" => "h5 span",
            "PROPERTY_VALUE" => "ul li",
            "ROOM_LIST" => "#rooms_table tbody tr",
            "ROOM_ID" => "td.roomType>div",
            "ROOM_NAME" => ".room-info .togglelink",
            "ROOM_CONFIG" => ".room-config li",
            "ROOM_REMOVE_H2" => "h2",
            "ROOM_REMOVE_SVG" => "span[data-name-en='Parking'] svg",
            "ROOM_REMOVE_I" => "span[data-name-en='Bathroom'] i",
            "ROOM_REMOVE_FOR_SIZE" => "p, h2, div, ul",
            "ROOM_SIZE_RIGHT" => ".hprt-lightbox-right-container",
            "ROOM_SIZE_LEFT" => ".hprt-lightbox-left-container",
            "ROOM_PARKING" => ".apt_block--right",
            "ROOM_BATHROOM" => "span[data-name-en='Bathroom']",
            "ROOM_PREVIEW_TEXT" => ".hprt-lightbox-right-container p:nth-child(4)",
            "ROOM_PREVIEW_TEXT_2" => ".hprt-lightbox-right-container p:nth-child(5)",
            "ROOM_IMAGES_LIST" => ".slick-slide",
            "ROOM_IMAGES" => "img",
            "ROOM_PROPERTY" => ".hprt-lightbox-list .hprt-lightbox-list__item",
            "ROOM_CAPABILITY_MULTI" => ".occupancy_multiplier>.occupancy_multiplier_number",
            "ROOM_CAPABILITY_CHILDREN" => ".occupancy_children .bicon-occupancychild",
            "ROOM_CAPABILITY" => ".occupancy_adults .bicon-occupancy",
        )
    );

    /*Вспомогательный метод. Убираем спец символы, пробелы и тд*/
    function trim_replace($line, $space = ""){

        $line = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    ', "•"), $space, trim($line));

        return $line;

    }

    /*Вспомогательный метод транслитации*/
    function trans($line)
    {
        $alf = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '',    'ы' => 'y',   'ъ' => '',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '',    'Ы' => 'Y',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );

        $line = strtr($line, $alf);
        $line = mb_strtolower($line);
        $line = mb_ereg_replace('[^-0-9a-z]', '_', $line);
        $line = mb_ereg_replace('[-]+', '_', $line);
        $line = trim($line, '-');

        return $line;
    }

    /*Вспомогательный метод для сортировки*/
    function unique_multi_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    /*Вспомогательный метод для отладки*/
    function pr($data){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    /*Вспомогательный метод для вывода ошибки и остановки скрипта*/
    protected function error($type, $error){

        if($type && $error){

            $ErrorData = "Error ".$type.": ".$error;
            $this->log .= $ErrorData;
            $this->log($this->log);
            $this->pr($ErrorData);
            exit();

        };
    }

    /*Вспомогательный метод для отправки логов/сообщения на почту*/
    function sendMail($email, $title, $text){

        if($email && $title && $text){

            $headers  = "Content-type: text/html; charset=windows-1251 \r\n";

            mail($email, $title, $text, $headers);

            $this->log .= date("d.m.Y H:i:s")." (".time().") Send email \r\n";

        }else{

            $this->error("SEND_MAIL", "Parameters are empty or incorrect");

        };

    }

    /*Вспомогательный метод для логирования*/
    function log($log){

        if($log){

            file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->upload_path."log.txt", $log, FILE_APPEND | LOCK_EX);

        }

    }

    /*Создаём URL для поиска*/
    function createFirstLink($type_allocation, $dest_type, $city_region){

        if($type_allocation && $dest_type && $city_region){

            $this->arResult["FIRST_LINK"] = $this->site_url.$this->site_search_page."nflt=";

            $type_allocation = array_filter(explode(";", $type_allocation));
            foreach ($type_allocation as $tAllocation){
                $this->arResult["FIRST_LINK"] .= urlencode("ht_id=".$tAllocation.";");
            };

            $this->arResult["FIRST_LINK"] .= "&ss=".urlencode($city_region);
            $this->arResult["FIRST_LINK"] .= "&dest_type=".$dest_type;
            $this->arResult["FIRST_LINK"] .= "&search_selected=true";

        }else{

            $this->error("CREATE_FIRST_LINK", "Parameters are empty or incorrect");

        };

        $selector = $this->selectorHotel["LIST_PAGE"];
        $link[0] = $this->arResult["FIRST_LINK"];
        $start_page = $this->getMultiCurl($link);

        $start_page_html = parent::newDocumentHTML($start_page[0]);

        $this->arResult["COUNT_PAGE"] = $start_page_html->find($selector["COUNT_PAGE"])->text();
        $this->arResult["COUNT_PAGE"] = ($this->arResult["COUNT_PAGE"])? $this->arResult["COUNT_PAGE"] : "1";

        $arResult = array(
            "COUNT_PAGE" => $this->arResult["COUNT_PAGE"],
            "FIRST_LINK" => $this->arResult["FIRST_LINK"]
        );

        return $arResult;

    }

    /*Мультикурл, при помощи данного метода парсим страницы букинга многопоточно*/
    function getMultiCurl($urls)
    {
        $conn = array();
        $arHtmlPage = array();

        if($urls){

            $mh = curl_multi_init();

            foreach ($urls as $i => $url) {
                $conn[$i] = curl_init($url);
                curl_setopt($conn[$i], CURLOPT_RETURNTRANSFER, 1); // возвращать результат
                curl_setopt($conn[$i], CURLOPT_FOLLOWLOCATION, 1); // если будет редирект - перейти по нему
                curl_setopt($conn[$i], CURLOPT_HEADER, 0); // не возвращать http-заголовок
                curl_setopt($conn[$i], CURLOPT_CONNECTTIMEOUT, 100); // таймаут соединения
                curl_setopt($conn[$i], CURLOPT_TIMEOUT, 100); // таймаут ожидания
                curl_setopt($conn[$i], CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5 GTB6");
                curl_setopt($conn[$i], CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($conn[$i], CURLOPT_SSL_VERIFYHOST, false);
                curl_multi_add_handle($mh, $conn[$i]);
            }

            do {
                $status = curl_multi_exec($mh, $active);

            } while ($status === CURLM_CALL_MULTI_PERFORM || $active);

            foreach ($urls as $i => $url) {
                $arHtmlPage[$i] = curl_multi_getcontent($conn[$i]);
                curl_multi_remove_handle($mh, $conn[$i]); // удаляем поток из мультикурла
                curl_close($conn[$i]); // закрываем отдельное соединение (поток)
            }

        }else{

            $this->error("GET_MULTI_CURL", "URL incorrect");

        }

        return $arHtmlPage;
    }

    /*Формируем линки на страницы со списком отелей*/
    function linkPageHotelList($url, $number_page, $quantity){

        if($url && $number_page && $quantity){

                $quantityPage = ($number_page!=1)? $number_page*$quantity-$quantity : $quantity;
                $this->arResult["HOTEL_LIST_LINK"][] = ($number_page!=1)? $url."&rows=".$quantity."&offset=".$quantityPage : $url."&rows=".$quantity;

        }else{

            $this->error("LINK_PAGE_HOTEL_LIST", "Parameters are empty or incorrect");

        }

        return $this->arResult["HOTEL_LIST_LINK"];

    }

    /*Добавляем ID отеля в json сохранённых мест прибывания/удаление дублей*/
    function setIDJsonBase($arrayHotelID, $linkJsonFile){
        if($arrayHotelID && $linkJsonFile){

            $arHotelsJsonFile = json_decode(file_get_contents($linkJsonFile), true);

            if($arHotelsJsonFile){
                $oneDimensionalArray = call_user_func_array('array_merge', $arHotelsJsonFile);
                foreach ($arrayHotelID as $key => $arID) {

                    if(in_array($arID, $oneDimensionalArray)){

                        unset($this->arResult["ITEMS"][$arID]);
                        unset($arrayHotelID[$key]);

                    }else{
                        $arJsonID[] = $arID;
                    };

                }

                if(!empty($arJsonID)) {
                    $arHotelsJsonFile[time()] = $arJsonID;
                    arsort($arHotelsJsonFile);
                    $resultHotelID = json_encode($arHotelsJsonFile, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);
                };

            }else{
                if(!empty($arrayHotelID)) {
                    $tempHotelID[time()] = $arrayHotelID;
                    $resultHotelID = json_encode($tempHotelID, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);
                };
            };

            if(!empty($resultHotelID)){
                file_put_contents($linkJsonFile, $resultHotelID);
            };

        }else{

            $this->error("SET_ID_JSON", "Parameters are empty or incorrect");

        };

    }

    /*Формируем линки на детальные страницы каждого отеля и получаем город*/
    function parserHotelList($hotelListHtml){

        if($hotelListHtml){

            $selector = $this->selectorHotel["LIST_PAGE"];
            $arHotelLink = array();

            foreach ($hotelListHtml as $file){

                $doc = parent::newDocumentHTML($file);

                $this->arResult["CITY"] = $doc->find($selector["CITY"])->attr("value");
                $this->arResult["CITY_CODE"] = $this->trans($this->trim_replace($this->arResult["CITY"]));

                $this->arResult["COUNT_PAGE"] = $doc->find($selector["COUNT_PAGE"])->text();
                $this->arResult["COUNT_PAGE"] = ($this->arResult["COUNT_PAGE"])? $this->arResult["COUNT_PAGE"] : "1";

                foreach ($doc->find($selector["LIST"]) as $page){

                    $page = pq($page);
                    $id = $this->trim_replace($page->attr("data-hotelid"));
                    $link = $this->trim_replace($page->find($selector["LINK"])->attr("href"));

                    $page->find($selector["LINK_REMOVE"])->remove();
                    $this->arResult["HOTEL_AREA"][$page->attr("data-hotelid")] = $this->trim_replace($page->find($selector["HOTEL_AREA"])->text());
                    $this->arResult["PREVIEW_TEXT"][$page->attr("data-hotelid")] = $this->trim_replace($page->find($selector["PREVIEW_TEXT"])->text());

                    if($link && !in_array($this->site_url.$link, $arHotelLink)){
                        $arHotelLink[] = $this->site_url.$link;
                        $this->arResult["HOTEL_LINK_FOR_ROOMS"][$id] = $this->site_url.$link;
                    };

                };

                $this->arResult["HOTEL_DETAIL_LINK"] = $arHotelLink;
            };

        }else{

            $this->error("PARSER_HOTEL_LIST", "HTML are empty or incorrect");

        }

        return $this->arResult["HOTEL_DETAIL_LINK"];

    }

    /*Получаем данные из html детальной страницы отеля*/
    function parserHotelDetail($HotelDetailHtml){

        if($HotelDetailHtml){

            $selector = $this->selectorHotel["DETAIL_PAGE"];

            foreach ($HotelDetailHtml as $file) {

                $arHotel = array();
                $doc = parent::newDocumentHTML($file);

                /*Получаем основные поля*/
                if($doc->find($selector["HOTEL_ID"])->attr("data-hotel-id")){

                    $arHotel["ID"] = $this->trim($doc->find($selector["HOTEL_ID"])->attr("data-hotel-id"));
                    $this->arResult["ID"][] = $arHotel["ID"];
                    $arHotel["TYPE"] = $this->trim($doc->find($selector["TYPE"])->text());
                    $arHotel["CITY"] = $this->trim_replace($doc->find($selector["CITY"])->attr("value"));
                    $arHotel["HOTEL_DETAIL_LINK"] = $this->arResult["HOTEL_LINK_FOR_ROOMS"][$arHotel["ID"]];

                    if($this->arResult["HOTEL_AREA"]){
                        $arHotel["HOTEL_AREA"] = $this->arResult["HOTEL_AREA"][$arHotel["ID"]];
                    };

                    $doc->find($selector["REMOVE_SPAN"])->remove();
                    $doc->find($selector["REMOVE_TOOLTIP"])->remove();

                    $arHotel["NAME"] = $this->trim($doc->find($selector["NAME"])->text());
                    $arHotel["ADDRESS"] = $this->trim($doc->find($selector["ADDRESS"])->text());
                    $arHotel["COORDINATES"] = $this->trim($doc->find($selector["COORDINATES"])->attr("data-atlas-latlng"));
                    $arHotel["HOTEL_STARS"] = $this->trim($doc->find($selector["STARS"])->text());

                    if($this->arResult["PREVIEW_TEXT"]) {
                        $arHotel["HOTEL_SHORT_DESCRIPTION"] = $this->trim_replace($this->arResult["PREVIEW_TEXT"][$arHotel["ID"]]);
                    };

                    $arHotel["DETAIL_TEXT"] = $this->trim_replace($doc->find($selector["DETAIL_TEXT"])->text());

                    /*Получаем основные изображения*/
                    if(!empty($doc->find($selector["TYPE_IMAGES"])->html())){
                        foreach ($doc->find($selector["IMAGES_SLIDER"]) as $image) {
                            $image = pq($image);
                            $href = $image->attr("href");
                            if($href != "#" && $href){
                                $imageID = $image->attr("data-photoid");
                                $hrefEx = explode("?", $href);
                                $this->arResult["IMAGES"][$imageID] = str_replace("max400", "max1024x768", $href);
                                $arHotel["IMAGES"][] = basename($hrefEx[0]);
                            };
                        }
                    }else{
                        foreach ($doc->find($selector["IMAGES_PLITKA"]) as $image) {
                            $image = pq($image);
                            $href = $image->attr("href");
                            if($href != "#" && $href){
                                $hrefEx = explode("?", $href);
                                $imageID = explode(".", basename($hrefEx[0]));
                                $this->arResult["IMAGES"][$imageID[0]] = str_replace("max400", "max1024x768", $href);
                                $arHotel["IMAGES"][] = basename($hrefEx[0]);
                            };
                        }
                    };

                    /*Получаем популярные удобства*/
                    foreach ($doc->find($selector["POPULAR_FACILITIES"]) as $popular) {
                        $popular = pq($popular);

                        $popular->find($selector["POPULAR_FACILITIES_REMOVE_SVG"])->remove();
                        $arHotel["HOTEL_POPULAR_FACILITIES"][] = $this->trim_replace($popular->text());
                    };

                    /*Получаем условия размещения*/
                    foreach ($doc->find($selector["CONDITIONS_LIST"]) as $condition) {
                        $condition = pq($condition);
                        $cnName = $this->trim_replace($condition->find($selector["CONDITIONS_NAME"])->text());
                        $cnCode = mb_strtoupper($this->trans($cnName));

                        $condition->find($selector["CONDITIONS_REMOVE"])->remove();

                        $cnValue = $this->trim_replace($condition->text(), " ");

                        switch ($this->type_file){
                            case "csv":

                                $arHotel["CONDITIONS"][$cnCode]["CODE"] = $cnCode;
                                $arHotel["CONDITIONS"][$cnCode]["NAME"] = $cnName;
                                $arHotel["CONDITIONS"][$cnCode]["VALUE"] = $cnValue;

                                if($cnCode == "KARTY__KOTORYE_PRINIMAET_OTEL" || $cnCode == "KARTY__KOTORYE_PRINIMAET_OBEKT_RAZMESCHENIYA"){
                                    foreach ($doc->find($selector["CONDITIONS_CARD"]) as $card){
                                        $card = pq($card);
                                        $arHotel["CONDITIONS"][$cnCode]["VALUE_ENUM"][] = $card->attr("aria-label");
                                    };
                                }

                                break;

                            case "json":
                                if($cnCode){
                                    if($cnCode == "KARTY__KOTORYE_PRINIMAET_OTEL" || $cnCode == "KARTY__KOTORYE_PRINIMAET_OBEKT_RAZMESCHENIYA"){
                                        foreach ($doc->find($selector["CONDITIONS_CARD"]) as $card){
                                            $card = pq($card);
                                            $arHotel["CONDITIONS_".$cnCode][] = $card->attr("aria-label");
                                        };
                                        $arHotel["CONDITIONS_HOTEL_CARDS_DESCRIPTION"] = $cnValue;
                                    }else{
                                        $arHotel["CONDITIONS_".$cnCode] = $cnValue;
                                    };
                                };

                                break;
                        }
                    };

                    /*Получаем свойства*/
                    foreach ($doc->find($selector["PROPERTY_LIST"]) as $properties) {
                        $properties = pq($properties);
                        $prName = $this->trim_replace($properties->find($selector["PROPERTY_NAME"])->text());
                        $prCode = mb_strtoupper($this->trans($prName));

                        if(!in_array($prCode, $this->arResult["PROPERTIES_TYPE"])){
                            $this->arResult["PROPERTIES_TYPE"][] = $prCode;
                        };

                        $properties->find($selector["PROPERTY_REMOVE_SPAN_NAME"])->remove();
                        $arCountProperties = $properties->find($selector["PROPERTY_VALUE"])->count();

                        switch ($this->type_file){

                            case "csv":

                                if($arCountProperties > 1){
                                    foreach ($properties->find($selector["PROPERTY_VALUE"]) as $prValue) {
                                        $prValue = pq($prValue);
                                        $value = $this->trim_replace($prValue->text(), " ");
                                        $arHotel["PROPERTIES"][$prCode]["VALUE"][] = $this->trim($value);
                                    };
                                }else{
                                    $arHotel["PROPERTIES"][$prCode]["VALUE"][] = $this->trim_replace($properties->find($selector["PROPERTY_VALUE"])->text(), " ");
                                };


                                break;

                            case "json":
                                $arHotel["PROPERTIES"][$prCode]["NAME"] = $prName;
                                if($arCountProperties > 1){
                                    foreach ($properties->find($selector["PROPERTY_VALUE"]) as $prValue) {
                                        $prValue = pq($prValue);
                                        $value = $this->trim_replace($prValue->text(), " ");
                                        $arHotel["PROPERTIES"][$prCode]["VALUE"][] = $this->trim($value);
                                    };
                                }else{
                                    $arHotel["PROPERTIES"][$prCode]["VALUE"][] = $this->trim_replace($properties->find($selector["PROPERTY_VALUE"])->text(), " ");
                                };

                                break;
                        }
                    };

                    /*Получаем комнаты*/
                    $rmID = 0;
                    foreach ($doc->find($selector["ROOM_LIST"]) as $room) {

                        $room = pq($room);
                        $rmCurrentID = $room->find($selector["ROOM_ID"])->attr("id");
                        $rmID = ($rmCurrentID) ? $rmCurrentID : $rmID;

                        if ($rmCurrentID) {
                            $arHotel["ROOMS"][$rmID]["ID"] = $rmID;
                            $arHotel["ID_CLICK"][$rmID] = "RD".$rmID;
                            $arHotel["ROOMS"][$rmID]["PARENT_ID"] = $arHotel["ID"];
                            $arHotel["ROOMS"][$rmID]["NAME"] = $this->trim_replace($room->find($selector["ROOM_NAME"])->text());
                            $arHotel["ROOMS"][$rmID]["EN_NAME"] = $this->trim_replace($room->find($selector["ROOM_NAME"])->attr("data-room-name-en"));

                            foreach ($room->find($selector["ROOM_CONFIG"]) as $rmConfig) {
                                $rmConfig = pq($rmConfig);
                                $arHotel["ROOMS"][$rmID]["CONFIG"][] = $this->trim_replace($rmConfig->find("strong")->text()) . " " . $this->trim_replace($rmConfig->find("span")->text());
                            };

                            if($room->find($selector["ROOM_CAPABILITY_MULTI"])->text()){

                                $arHotel["ROOMS"][$rmID]["CAPABILITY"] = $room->find($selector["ROOM_CAPABILITY_MULTI"])->text();

                            }else{

                                $arHotel["ROOMS"][$rmID]["CAPABILITY"] = $room->find($selector["ROOM_CAPABILITY"])->count();

                            };

                            $arHotel["ROOMS"][$rmID]["CAPABILITY_CHILDREN"] = $room->find($selector["ROOM_CAPABILITY_CHILDREN"])->count();


                        } else {
                            //$arHotel["ROOMS"][$rmID]["POPUP_LINK"] = $this->arResult["HOTEL_LINK_FOR_ROOMS"][$arHotel["ID"]]."#room_".$rmID;
                            /*$room->find($selector["ROOM_REMOVE_H2"])->remove();
                            $room->find($selector["ROOM_REMOVE_SVG"])->remove();
                            $room->find($selector["ROOM_REMOVE_I"])->remove();

                            $arHotel["ROOMS"][$rmID]["ROOM_SIZE"] = $this->trim_replace($room->find($selector["ROOM_SIZE"])->text());
                            $arHotel["ROOMS"][$rmID]["PARKING"] = $this->trim_replace($room->find($selector["ROOM_PARKING"])->text(), " ");
                            $arHotel["ROOMS"][$rmID]["BATHROOM"] = $this->trim_replace($room->find($selector["ROOM_BATHROOM"])->text(), " ");
                            $arHotel["ROOMS"][$rmID]["PREVIEW_TEXT"] = $this->trim_replace($room->find($selector["ROOM_PREVIEW_TEXT"])->text(), " ");


                            foreach ($room->find($selector["ROOM_IMAGES_LIST"]) as $imageRoom) {
                                $imageRoom = pq($imageRoom);
                                $imageID = $imageRoom->attr("data-photoid");
                                $this->arResult["IMAGES"][$imageID] = $imageRoom->find($selector["ROOM_IMAGES"])->attr("data-lazy");
                                $arHotel["ROOMS"][$rmID]["IMAGES"][] = basename($imageRoom->find($selector["ROOM_IMAGES"])->attr("data-lazy"));
                            }

                            foreach ($room->find($selector["ROOM_PROPERTY"]) as $propertiesRoom) {
                                $propertiesRoom = pq($propertiesRoom);
                                $prName = $this->trim($this->trim_replace($propertiesRoom->text()));
                                $prCode = mb_strtoupper($this->trans($propertiesRoom->attr("data-name-en")));
                                $arHotel["ROOMS"][$rmID]["PROPERTIES"][$prCode] = $prName;
                            };*/

                        };
                    };

                    $this->arResult["ITEMS"][$arHotel["ID"]] = $arHotel;
                };
            };

        }else{

            $this->error("PARSER_HOTEL_DETAIL", "HTML are empty or incorrect");

        };

        return $this->arResult["ITEMS"];

    }
    /*Динамические данные комнаты отеля*/
    function parserHotelRooms(){

        $selector = $this->selectorHotel["DETAIL_PAGE"];

        foreach ($this->arResult["ITEMS"] as $hotel_id => $item) {
            foreach ($item["ROOMS"] as $room_id => $rooms) {

                $arRooms= array();
                $room = pq($rooms["HTML_ROOMS"]);

                $room->find($selector["ROOM_REMOVE_H2"])->remove();
                $room->find($selector["ROOM_REMOVE_SVG"])->remove();
                $room->find($selector["ROOM_REMOVE_I"])->remove();

                $arRooms["PARKING"] = $this->trim_replace($room->find($selector["ROOM_PARKING"])->text(), " ");
                $arRooms["BATHROOM"] = $this->trim_replace($room->find($selector["ROOM_BATHROOM"])->text(), " ");
                $arRooms["PREVIEW_TEXT"] = $this->trim_replace($room->find($selector["ROOM_PREVIEW_TEXT"])->text(), " ");
                if(!$arRooms["PREVIEW_TEXT"]){
                    $arRooms["PREVIEW_TEXT"] = $this->trim_replace($room->find($selector["ROOM_PREVIEW_TEXT_2"])->text(), " ");
                };

                foreach ($room->find($selector["ROOM_IMAGES_LIST"]) as $imageRoom) {
                    $imageRoom = pq($imageRoom);
                    $imageID = $imageRoom->attr("data-photoid");
                    $src = ($imageRoom->find($selector["ROOM_IMAGES"])->attr("src")) ? $imageRoom->find($selector["ROOM_IMAGES"])->attr("src") : $imageRoom->find($selector["ROOM_IMAGES"])->attr("data-lazy");
                    $hrefEx = explode("?", $src);
                    $this->arResult["IMAGES"][$imageID] = $src;
                    $arRooms["IMAGES"][] = basename($hrefEx[0]);
                }

                foreach ($room->find($selector["ROOM_PROPERTY"]) as $key => $propertiesRoom) {
                    $propertiesRoom = pq($propertiesRoom);
                    $prName = $this->trim($this->trim_replace($propertiesRoom->text()));
                    $prCode = mb_strtoupper($this->trans($propertiesRoom->attr("data-name-en")));
                    $arRooms["PROPERTIES"][$prCode] = $prName;
                    //$arRooms["PROPERTIES"][$key] = $propertiesRoom->text();
                };

                $room->find(".hprt-lightbox-right-container h2, .hprt-lightbox-right-container p, .hprt-lightbox-right-container ul, .hprt-lightbox-right-container div")->remove();
                $arRooms["ROOM_SIZE"] = $this->trim_replace($room->find($selector["ROOM_SIZE_RIGHT"])->text());

                if(!$arRooms["ROOM_SIZE"]){
                    $room->find(".hprt-lightbox-left-container h2, .hprt-lightbox-left-container p, .hprt-lightbox-left-container ul, .hprt-lightbox-left-container div")->remove();
                    $arRooms["ROOM_SIZE"] = $this->trim_replace($room->find($selector["ROOM_SIZE_LEFT"])->text());
                };

                $this->arResult["ITEMS"][$hotel_id]["ROOMS"][$room_id] = array_merge($rooms, $arRooms);
                unset($this->arResult["ITEMS"][$hotel_id]["ROOMS"][$room_id]["HTML_ROOMS"]);
            }
        }

    }

    /*Запуск хэдлес браузера и извлечение динамического контента*/
    function headlessBrowser($link_browser){

        $browserFactory = new BrowserFactory($link_browser);
        $browser = $browserFactory->createBrowser([
            'enableImages' => false,
            'connectionDelay' => 600,
        ]);

        foreach ($this->arResult["ITEMS"] as &$item) {
            //foreach ($item["ROOMS"] as &$rooms) {

                $page = $browser->createPage();

                $page->navigate($item["HOTEL_DETAIL_LINK"])->waitForNavigation('networkIdle');

                foreach ($item["ID_CLICK"] as $id_rooms => $click_id) {

                    $page->evaluate('document.querySelector("#'.$click_id.' a").click()');

                    $html = $page
                        ->evaluate('document.querySelector(".hp_rt_lightbox_content").innerHTML')
                        ->getReturnValue();

                    $page->evaluate('document.querySelector(".lightbox_close_button").click()');
                    $this->arResult["ITEMS"][$item["ID"]]["ROOMS"][$id_rooms]["HTML_ROOMS"] = $html;
                };

                $page->close();

            //}
        }

        $browser->close();
    }

    /*Создаём CSV структуру для минимального набора данных*/
    function createMinStructure($upload_path, $type_file){

        if($upload_path && $type_file){

            $this->arResult["STRUCTURE"] =array(
                "FILE" => $_SERVER['DOCUMENT_ROOT'].$upload_path."import.".$type_file
            );

            if (!file_exists($this->arResult["STRUCTURE"]["FILE"])){
                fopen($this->arResult["STRUCTURE"]["FILE"] , "w");
            };

        }else{

            $this->error("CREATE_FILE_STRUCTURE", "Parameters are empty or incorrect");

        }

        return $this->arResult["STRUCTURE"];
    }

    /*Создаём CSV/Json структуру*/
    function createFileStructure($city, $upload_path, $type_file, $properties){

        if($upload_path && $city && $type_file){

            //$dir = $upload_path.$type_file."_".$city."_hotel/";
            $dir = $_SERVER['DOCUMENT_ROOT'].$upload_path."import/";

            switch ($type_file) {

                case "csv":

                    $this->arResult["STRUCTURE"] =array(
                        "DIR" => $dir,
                        "DIR_IMAGES"=>$dir."images/",
                        "FILE" => array(
                            "HOTEL" => $dir."hotel.".$type_file,
                            "CONDITIONS_HOTEL" => $dir."hotel_conditions.".$type_file,
                            "PROPERTIES_HOTEL" => $dir."hotel_properties.".$type_file,
                            "ROOMS" => $dir."rooms.".$type_file
                        )
                    );

                    break;

                case "json":

                    $this->arResult["STRUCTURE"] = array(
                        "DIR" => $dir,
                        "DIR_IMAGES"=>$dir."images/",
                        "FILE" => array(
                            "HOTEL" => $dir."hotel.".$type_file,
                            "ROOMS" => $dir."rooms.".$type_file
                        )
                    );

                    if($properties){

                        foreach ($properties as $prop){
                            $this->arResult["STRUCTURE"]["FILE"][$prop] = $dir."hotel_properties_".mb_strtolower($prop).".".$type_file;
                        };

                    };

                    break;
            }

            if (!file_exists($dir)){
                mkdir($this->arResult["STRUCTURE"]["DIR_IMAGES"], 0777, true);
                foreach ($this->arResult["STRUCTURE"]["FILE"] as $file){
                    fopen($file, "w");
                };
            };

        }else{

            $this->error("CREATE_FILE_STRUCTURE", "Parameters are empty or incorrect");

        }

        return $this->arResult["STRUCTURE"];

    }

    /*Записываем минимальные данные в CSV*/
    function writeMinCSV($linkFile, $arHotels){

        if($linkFile && $arHotels){
            $resultHotel = array();
            $hotelCSV[] = array("ID", "CITY", "NAME", "ADDRESS");

            foreach ($arHotels as $hotel){

                $hotelCSV[] = array(
                    "ID" => $hotel["ID"],
                    "CITY" => $hotel["CITY"],
                    "NAME" => $hotel["NAME"],
                    "ADDRESS" => $hotel["ADDRESS"]
                );

            };

            $createCSV = array(
                "HOTEL" => array(
                    "LINK_CSV" => $linkFile,
                    "DATA" => $hotelCSV,
                )
            );

            foreach ($createCSV as $csv){
                $csvFile = fopen($csv["LINK_CSV"], "a") or die("Unable to open file!");

                if(filesize($csv["LINK_CSV"])){
                    $tempMinData = array();
                    unset($csv["DATA"][0]);
                    if (($handle = fopen($csv["LINK_CSV"], "r")) !== FALSE) {
                        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                            $temp["ID"] = $data[0];
                            $temp["CITY"] = $data[1];
                            $temp["NAME"] = $data[2];
                            $temp["ADDRESS"] = $data[3];
                            $tempMinData[] = $temp;
                        }
                        fclose($handle);
                    }
                    unset($tempMinData[0]);
                    $resultHotel = $this->unique_multi_array(array_merge($tempMinData, $csv["DATA"]), "ID");
                    array_unshift($resultHotel, array("ID", "CITY", "NAME", "ADDRESS"));
                    file_put_contents($linkFile, '');
                }else{
                    $resultHotel = $csv["DATA"];
                };

                foreach ($resultHotel as $row) {
                    fputcsv($csvFile, $row);
                }
                fclose($csvFile);
            };

        }else{

            $this->error("WRITE_CSV", "Parameters are empty or incorrect");

        }

    }

    /*Записываем данные в Json*/
    function writeJson($linkDir, $arHotels){

        if($linkDir && $arHotels){

            $hotels = array();
            $rooms = array();
            $properties = array();
            foreach ($arHotels as $hotel){
                foreach ($hotel["ROOMS"] as $htRooms) {
                    $rooms[] = $htRooms;
                }
                foreach ($hotel["PROPERTIES"] as $keyProp => $property){
                    foreach ($property["VALUE"] as $keyValue => $propValue){

                        $prCode = mb_strtoupper($this->trans($propValue));
                        $properties[$keyProp][] = array(
                            "ID" => $hotel["ID"].$keyValue,
                            "PROPERTY_ID" => $hotel["ID"],
                            $keyProp => $property["NAME"],
                            "VALUE" => $propValue
                        );

                    };
                }
                unset($hotel["ROOMS"]);
                unset($hotel["PROPERTIES"]);
                $hotels[] = $hotel;


            }
            if($linkDir && $arHotels){

                $arHotelsJsonFile = file_get_contents($linkDir["HOTEL"]);
                $arRoomsJsonFile = file_get_contents($linkDir["ROOMS"]);


                if(!empty($arHotelsJsonFile) && !empty($arRoomsJsonFile)){

                    $tempHotel = json_decode($arHotelsJsonFile, true);
                    $tempRooms = json_decode($arRoomsJsonFile, true);

                    $resultHotel = $this->unique_multi_array(array_merge($tempHotel, $hotels), "ID");
                    $resultRooms = $this->unique_multi_array(array_merge($tempRooms, $rooms), "ID");

                }else{

                    $resultHotel = $hotels;
                    $resultRooms = $rooms;

                };


                $arHotelsJson = json_encode($resultHotel, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK);
                $arRoomsJson = json_encode($resultRooms, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK);

                file_put_contents($linkDir["HOTEL"], $arHotelsJson);
                file_put_contents($linkDir["ROOMS"], $arRoomsJson);

                foreach ($properties as $prKey => $prop){
                    $arPropertiesJsonFile = json_decode(file_get_contents($linkDir[$prKey]), true);
                    if($arPropertiesJsonFile){
                        $arProperties = json_encode($this->unique_multi_array(array_merge($arPropertiesJsonFile, $prop), "ID"), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK);
                    }else{
                        $arProperties = json_encode($prop, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                    };
                    file_put_contents($linkDir[$prKey], $arProperties);
                };

            };
        }else{

            $this->error("WRITE_JSON", "Parameters are empty or incorrect");

        }
    }

    /*Записываем данные в CSV*/
    function writeCSV($linkDir, $arHotels){

        if($linkDir && $arHotels){

            $hotelCSV[] = array("ID", "TYPE", "NAME", "STARS", "HOTEL_AREA", "ADDRESS", "COORDINATES","PREVIEW_TEXT", "DETAIL_TEXT", "POPULAR_FACILITIES", "IMAGES");
            $hotelConditionsCSV[] = array("ID_PARENT", "CODE", "NAME", "VALUE", "VALUE_ENUM");
            $hotelPropertiesCSV[] = array("ID_PARENT", "CODE", "NAME", "VALUE");
            $roomCSV[] = array("ID", "ID_PARENT", "NAME_PARENT", "NAME", "EN_NAME", "ROOM_SIZE", "PARKING", "BATHROOM", "CONFIG", "PROPERTIES", "IMAGES");

            foreach ($arHotels as $hotel){

                $hotelCSV[] = array(
                    "ID" => $hotel["ID"],
                    "TYPE" => $hotel["TYPE"],
                    "NAME" => $hotel["NAME"],
                    "STARS" => $hotel["HOTEL_STARS"],
                    "HOTEL_AREA" => $hotel["HOTEL_AREA"],
                    "ADDRESS" => $hotel["ADDRESS"],
                    "COORDINATES" => $hotel["COORDINATES"],
                    "PREVIEW_TEXT" => $hotel["HOTEL_SHORT_DESCRIPTION"],
                    "DETAIL_TEXT" => $hotel["DETAIL_TEXT"],
                    "POPULAR_FACILITIES" => ($hotel["HOTEL_POPULAR_FACILITIES"]) ? implode(";", $hotel["HOTEL_POPULAR_FACILITIES"]) : $hotel["HOTEL_POPULAR_FACILITIES"],
                    "IMAGES" => ($hotel["IMAGES"]) ? implode(";", $hotel["IMAGES"]) : $hotel["IMAGES"]
                );
                if($hotel["CONDITIONS"]){
                    foreach ($hotel["CONDITIONS"] as $htConditions){
                        $hotelConditionsCSV[] = array(
                            "ID_PARENT" => $hotel["ID"],
                            "CODE" => $htConditions["CODE"],
                            "NAME" => $htConditions["NAME"],
                            "VALUE" => $htConditions["VALUE"],
                            "VALUE_ENUM" => ($htConditions["VALUE_ENUM"]) ? implode(";", $htConditions["VALUE_ENUM"]) : $htConditions["VALUE_ENUM"],
                        );
                    };
                }
                if($hotel["PROPERTIES"]){
                    foreach ($hotel["PROPERTIES"] as $htProperties){
                        $hotelPropertiesCSV[] = array(
                            "ID_PARENT" => $hotel["ID"],
                            "CODE" => $htProperties["CODE"],
                            "NAME" => $htProperties["NAME"],
                            "VALUE" => ($htProperties["VALUE"]) ? implode(";", $htProperties["VALUE"]) : $htProperties["VALUE"]
                        );
                    };
                }
                if($hotel["ROOMS"]) {
                    foreach ($hotel["ROOMS"] as $htRooms) {
                        $roomCSV[] = array(
                            "ID" => $htRooms["ID"],
                            "ID_PARENT" => $hotel["ID"],
                            "NAME_PARENT" => $hotel["NAME"],
                            "NAME" => $htRooms["NAME"],
                            "EN_NAME" => $htRooms["EN_NAME"],
                            "ROOM_SIZE" => $htRooms["ROOM_SIZE"],
                            "PARKING" => $htRooms["PARKING"],
                            "BATHROOM" => $htRooms["BATHROOM"],
                            "CONFIG" => ($htRooms["CONFIG"]) ? implode(";", $htRooms["CONFIG"]) : $htRooms["CONFIG"],
                            "PROPERTIES" => ($htRooms["PROPERTIES"]) ? implode(";", $htRooms["PROPERTIES"]) : $htRooms["PROPERTIES"],
                            "IMAGES" => ($htRooms["IMAGES"]) ? implode(";", $htRooms["IMAGES"]) : $htRooms["IMAGES"]
                        );
                    };
                }
            };

            $createCSV = array(
                "HOTEL" => array(
                    "LINK_CSV" => $linkDir["FILE"]["HOTEL"],
                    "DATA" => $hotelCSV,
                ),
                "CONDITIONS" => array(
                    "LINK_CSV" => $linkDir["FILE"]["CONDITIONS_HOTEL"],
                    "DATA" => $hotelConditionsCSV,
                ),
                "PROPERTIES" => array(
                    "LINK_CSV" => $linkDir["FILE"]["PROPERTIES_HOTEL"],
                    "DATA" => $hotelPropertiesCSV,
                ),
                "ROOMS" => array(
                    "LINK_CSV" => $linkDir["FILE"]["ROOMS"],
                    "DATA" => $roomCSV,
                )
            );

            foreach ($createCSV as $csv){
                $csvFile = fopen($csv["LINK_CSV"], "a") or die("Unable to open file!");

                if(filesize($csv["LINK_CSV"])){
                    unset($csv["DATA"][0]);
                }

                foreach ($csv["DATA"] as $row) {
                    fputcsv($csvFile, $row);
                }
                fclose($csvFile);
            };

        }else{

            $this->error("WRITE_CSV", "Parameters are empty or incorrect");

        }
    }

    /*Сохраняем изображения*/
    function parsesImg($arImage, $urlUpload){

        if($arImage && $urlUpload){
            foreach ($arImage as $key => $image){
                $hrefEx = explode("?", $image);
                $imgName = basename($hrefEx[0]);
                copy($image, $urlUpload.$imgName);
            };
        }else{

            $this->error("PARSER_IMG", "Parameters are empty or incorrect");

        }

    }
    function clearDirectory($path){
        if(file_exists($path) && is_dir($path)){
            array_map('unlink', glob($path."*"));
        };
    }

    /*Основной метод сборки парсинга c минимальными данными*/
    function parsingMinData($link, $page, $quantity, $upload_path, $type_file){

        if($link){

            $this->log = (date("H:i:s")." Start parsing \r\n");

            if($page && $quantity){
                $this->page = $page;
                $this->quantity = $quantity;
            }
            if(!empty($upload_path)){
                $this->upload_path = $upload_path;
            }

            $linkPageHotelList = $this ->linkPageHotelList($link, $this->page, $this->quantity);
            $this->log .= date("H:i:s")." Get links list hotels \r\n";

            $hotelListHtml = $this ->getMultiCurl($linkPageHotelList);
            $this->log .= date("H:i:s")." Get html list hotels \r\n";

            $hotelDetailLink = $this ->parserHotelList($hotelListHtml);
            $this->log .= date("H:i:s")." Get links detail hotels \r\n";

            $hotelDetailHtml = $this ->getMultiCurl($hotelDetailLink);
            $this->log .= date("H:i:s")." Get html detail hotels \r\n";

            $arResultHotel = $this ->parserHotelDetail($hotelDetailHtml);
            $this->log .= date("H:i:s")." Parsing html detail hotels \r\n";

            $arStr = $this ->createMinStructure($this->upload_path, $type_file);
            $this->log .= date("H:i:s")." Create structure \r\n";

            $this ->writeMinCSV($arStr["FILE"], $arResultHotel);
            $this->log .= date("H:i:s")." Write data file \r\n";

            $this->log .= date("H:i:s")." Finish parsing \r\n";
            $this->log = "\r\n".$this->arResult["CITY_CODE"]." - ".$this->type_file." ".$page." page \r\n".$this->log;
            $this->log($this->log);

        }else{

            $this->error("PARSING_DATA", "Parameters are empty or incorrect");

        }

        $this->log = str_replace("\r\n", "<br>", $this->log);
        return $this->log;

    }

    /*Основной метод сборки парсинга*/
    function parsingData($link, $page, $quantity, $upload_path, $upload_img, $type_file){

        if($link){

            $this->log = date("d.m.Y H:i:s")." (".time().") Start parsing \r\n";

            if($page && $quantity){
                $this->page = $page;
                $this->quantity = $quantity;
            }
            if(!empty($upload_path)){
                $this->upload_path = $upload_path;
            }
            if(!empty($type_file) && in_array($type_file, $this->arTypeFile)){
                $this->type_file = $type_file;
            }
            if(!empty($upload_img)){
                $this->upload_img = $upload_img;
            }

            $linkPageHotelList = $this ->linkPageHotelList($link, $this->page, $this->quantity);
            $this->log .= date("d.m.Y H:i:s")." (".time().") Get links list hotels \r\n";

            $hotelListHtml = $this ->getMultiCurl($linkPageHotelList);
            $this->log .= date("d.m.Y H:i:s")." (".time().") Get html list hotels \r\n";

            $hotelDetailLink = $this ->parserHotelList($hotelListHtml);
            $this->log .= date("d.m.Y H:i:s")." (".time().") Get links detail hotels \r\n";

            $hotelDetailHtml = $this ->getMultiCurl($hotelDetailLink);
            $this->log .= date("d.m.Y H:i:s")." (".time().") Get html detail hotels \r\n";

            $this ->parserHotelDetail($hotelDetailHtml);
            $this->log .= date("d.m.Y H:i:s")." (".time().") Parsing html detail hotels \r\n";

            $this->headlessBrowser('C:\\OSPanel\\domains\\dev.test.by\\ibby_parser\\chrome\\chrome.exe');
            $this->log .= date("d.m.Y H:i:s")." (".time().") headless Browser get html \r\n";

            $this->parserHotelRooms();
            $this->log .= date("d.m.Y H:i:s")." (".time().") Parsing html detail rooms \r\n";

            $this ->setIDJsonBase($this->arResult["ID"], $_SERVER['DOCUMENT_ROOT'].$this->linkJsonFileID);
            $this->log .= date("d.m.Y H:i:s")." (".time().") Set ID hotel/remove duplicate \r\n";

            $arStr = $this ->createFileStructure($this->arResult["CITY_CODE"], $this->upload_path, $this->type_file, $this->arResult["PROPERTIES_TYPE"]);
            $this->log .= date("d.m.Y H:i:s")." (".time().") Create structure \r\n";

            if($page == 1){
                $this->clearDirectory($arStr["DIR_IMAGES"]);
                $this->clearDirectory($arStr["DIR"]);
                $this->log .= date("d.m.Y H:i:s")." (".time().") Remove files ".$arStr["DIR"]." \r\n";
            };

            switch ($this->type_file) {

                case "csv":
                    $this ->writeCSV($arStr, $this->arResult["ITEMS"]);
                break;

                case "json":
                    $this ->writeJson($arStr["FILE"], $this->arResult["ITEMS"]);
                break;

            }
            $this->log .= date("d.m.Y H:i:s")." (".time().") Write data file \r\n";

            if(!empty($this->upload_img) && $this->upload_img != "false"){
                $this ->parsesImg($this->arResult["IMAGES"], $arStr["DIR_IMAGES"]);
                $this->log .= date("d.m.Y H:i:s")." Save images \r\n";
            };

            $this->log .= date("d.m.Y H:i:s")." (".time().") Finish parsing \r\n";

            if($page == $this->arResult["COUNT_PAGE"]){
                $this->sendMail("bertuzoa@gmail.com", "Finish parsing ".$this->arResult["CITY_CODE"], $this->log);
            };

            $this->log = "\r\n".$this->arResult["CITY_CODE"]." - ".$this->type_file." ".$page." page \r\n".$this->log;

            $this->log($this->log);

        }else{

            $this->error("PARSING_DATA", "Parameters are empty or incorrect");

        }
        $this->log = str_replace("\r\n", "<br>", $this->log);
        return $this->log;
    }
};

