<?php
error_reporting(0);

require($_SERVER['DOCUMENT_ROOT'] . '/ibby_parser/parserBooking.php');

$parser = new parserBooking();

if($_REQUEST["mindata"]){

    if($_REQUEST["type_allocation"] && $_REQUEST["dest_type"] && $_REQUEST["city_region"]){

        $arMessage = $parser -> createFirstLink($_REQUEST["type_allocation"], $_REQUEST["dest_type"], $_REQUEST["city_region"]);

    }else if($_REQUEST["page"] && $_REQUEST["url"] && $_REQUEST["item"] && $_REQUEST["upload_path"]){

        $arMessage = $parser -> parsingMinData(
            $_REQUEST["url"],
            $_REQUEST["page"],
            $_REQUEST["item"],
            $_REQUEST["upload_path"],
            $_REQUEST["type_file"]
        );

    }else{

        $arMessage["ERROR"] = "Empty items";

    };

}else{

    if($_REQUEST["type_allocation"] && $_REQUEST["dest_type"] && $_REQUEST["city_region"]){

        $arMessage = $parser -> createFirstLink($_REQUEST["type_allocation"], $_REQUEST["dest_type"], $_REQUEST["city_region"]);

    }else if($_REQUEST["page"] && $_REQUEST["url"] && $_REQUEST["item"] && $_REQUEST["upload_path"]){

        $arMessage = $parser -> parsingData(
            $_REQUEST["url"],
            $_REQUEST["page"],
            $_REQUEST["item"],
            $_REQUEST["upload_path"],
            $_REQUEST["upload_img"],
            $_REQUEST["type_file"]
        );

    }else{

        $arMessage["ERROR"] = "Empty items";

    };

};
echo json_encode($arMessage);